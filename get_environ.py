import gitlab
import argparse
import yaml
import requests

def get_merged_ci_file(gitlab, headers, parsed_ci_content):
    data = {'content': parsed_ci_content}
    try:
        response = requests.post( gitlab + "api/v4/ci/lint?include_merged_yaml=true", headers = headers, data = data)
        response_body = response.json()
        if response_body["status"] == "valid":
            return response_body["merged_yaml"]
        else:
            print("YML invalid")
            return None
    except Exception as err:
        print("Could not lint CI file:")
        print(err)
        return None

parser = argparse.ArgumentParser(description='Get environment by job id')
parser.add_argument('gitlab', help='GitLab url')
parser.add_argument('token', help='API token able to read the requested project')
parser.add_argument('project', help='Project ID of the requested job')
parser.add_argument('job', help='Job ID to determine environment for')
args = parser.parse_args()

gitlab_url = args.gitlab if args.gitlab.endswith("/") else args.gitlab + "/"
gl = gitlab.Gitlab(gitlab_url, private_token=args.token)

headers = {'PRIVATE-TOKEN': args.token}

project = gl.projects.get(args.project)
job = project.jobs.get(args.job)
job_name = job.name

# determine which commit it was, so we can get the correct version of the ci file
commit_sha = job.commit["id"]

# this assumes default naming of the ci file which may not apply to all pipelines!
ci_file = project.files.get(file_path='.gitlab-ci.yml', ref=commit_sha)
ci_file_content = ci_file.decode()

#get a merged CI yml file to resolve includes and variables
merged_file = get_merged_ci_file(gitlab_url, headers, ci_file_content)
#the merged file returns keys with a prefixed ":", so we will have to account for that in the script
job_name = ":" + job_name
#parse the ci yml
parsed_ci = yaml.safe_load(merged_file)
print(parsed_ci)
# get the job's definition from the yml, then get the defined environment

if job_name in parsed_ci:
    job_definition = parsed_ci[job_name]
    if ":environment" in job_definition:
        environment = job_definition[":environment"]
        if ":name" in environment:
            print("Environment name %s" % environment[":name"])
        if ":url" in environment:
            print("Environment url %s" % environment[":url"])
    else:
        print("No environment defined for %s at ref %s" % (job_name, commit_sha))
else:
    print("[Error] Couldn't find job %s in ci file at ref %s" % (job_name, commit_sha))
