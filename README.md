# Get job environment via API

Get a job's environment via GitLab API, given project ID and job ID

The script will retrieve a job via API, use its `commit.id` to retrieve the correct version of its `gitlab-ci.yml`, sends it to the [CI Lint API](https://docs.gitlab.com/ee/api/lint.html) to retrieve [an expanded yml](https://docs.gitlab.com/ee/api/lint.html#yaml-expansion) that resolves includes. It then parses that expanded yml file, retrieving the job's definition via its `name`, then accesses the `environment` attributes defined in the job.

# Limitations

The script assumes that the CI file used is the standard ´.gitlab-ci.yml´ file in the root of the project.

This script is not well tested and exceptions are not handled. It's more of a proof of concept.

# Usage

`pip install -r requirements.txt` 
`python3 get_environ.py $GITLAB_URL $GIT_TOKEN $PROJECT_ID $JOB_ID`

The token you use needs `read_api` **and** `read_repository` scope because it accesses the `gitlab-ci.yml` as a repository file.

# Disclaimer

This script is provided for educational purposes. It is not supported by GitLab. However, you can create an issue if you need help or propose a Merge Request. Please don't use this in production unmodified, it will break.